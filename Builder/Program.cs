﻿using System.Threading;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using Gremlin.Net.Driver;
using Gremlin.Net.Driver.Exceptions;
using Gremlin.Net.Structure.IO.GraphSON;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

namespace Builder
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile("appsettings.local.json", true, true)
                .Build();

            var gremlinServer = new GremlinServer(config["GremlinDB:Host"], 443, enableSsl: true, 
                                                    username: $"/dbs/{config["GremlinDB:Database"]}/colls/{config["GremlinDB:Graph"]}", 
                                                    password: config["GremlinDB:Key"]);

            using (var gremlinClient = new GremlinClient(gremlinServer, new GraphSON2Reader(), new GraphSON2Writer(), GremlinClient.GraphSON2MimeType))
            {
                //clear out graph
                await gremlinClient.SubmitAsync("g.V().drop()");

                //start building the query
                StringBuilder sb = new StringBuilder("g");

                //create the chambers
                for(int index=0;index<20;++index)
                {
                    sb.Append($".addV('chamber').property('Name','Chamber #{index+1}').as('c{index+1}')");
                }

                //first ring: c1 - c5
                //right
                sb.Append(".addE('connects_to').property('direction','right').from('c1').to('c2')");
                sb.Append(".addE('connects_to').property('direction','right').from('c2').to('c3')");
                sb.Append(".addE('connects_to').property('direction','right').from('c3').to('c4')");
                sb.Append(".addE('connects_to').property('direction','right').from('c4').to('c5')");
                sb.Append(".addE('connects_to').property('direction','right').from('c5').to('c1')");
                //left
                sb.Append(".addE('connects_to').property('direction','left').from('c5').to('c4')");
                sb.Append(".addE('connects_to').property('direction','left').from('c4').to('c3')");
                sb.Append(".addE('connects_to').property('direction','left').from('c3').to('c2')");
                sb.Append(".addE('connects_to').property('direction','left').from('c2').to('c1')");
                sb.Append(".addE('connects_to').property('direction','left').from('c1').to('c5')");
                //up
                sb.Append(".addE('connects_to').property('direction','up').from('c1').to('c6')");
                sb.Append(".addE('connects_to').property('direction','up').from('c2').to('c8')");
                sb.Append(".addE('connects_to').property('direction','up').from('c3').to('c10')");
                sb.Append(".addE('connects_to').property('direction','up').from('c4').to('c12')");
                sb.Append(".addE('connects_to').property('direction','up').from('c5').to('c14')");


                //second ring c6 - c15
                //right
                sb.Append(".addE('connects_to').property('direction','right').from('c6').to('c7')");
                sb.Append(".addE('connects_to').property('direction','right').from('c7').to('c8')");
                sb.Append(".addE('connects_to').property('direction','right').from('c8').to('c9')");
                sb.Append(".addE('connects_to').property('direction','right').from('c9').to('c10')");
                sb.Append(".addE('connects_to').property('direction','right').from('c10').to('c11')");
                sb.Append(".addE('connects_to').property('direction','right').from('c11').to('c12')");
                sb.Append(".addE('connects_to').property('direction','right').from('c12').to('c13')");
                sb.Append(".addE('connects_to').property('direction','right').from('c13').to('c14')");
                sb.Append(".addE('connects_to').property('direction','right').from('c14').to('c15')");
                sb.Append(".addE('connects_to').property('direction','right').from('c15').to('c6')");
                //left
                sb.Append(".addE('connects_to').property('direction','left').from('c15').to('c14')");
                sb.Append(".addE('connects_to').property('direction','left').from('c14').to('c13')");
                sb.Append(".addE('connects_to').property('direction','left').from('c13').to('c12')");
                sb.Append(".addE('connects_to').property('direction','left').from('c12').to('c11')");
                sb.Append(".addE('connects_to').property('direction','left').from('c11').to('c10')");
                sb.Append(".addE('connects_to').property('direction','left').from('c10').to('c9')");
                sb.Append(".addE('connects_to').property('direction','left').from('c9').to('c8')");
                sb.Append(".addE('connects_to').property('direction','left').from('c8').to('c7')");
                sb.Append(".addE('connects_to').property('direction','left').from('c7').to('c6')");
                sb.Append(".addE('connects_to').property('direction','left').from('c6').to('c15')");
                //down
                sb.Append(".addE('connects_to').property('direction','down').from('c6').to('c1')");
                sb.Append(".addE('connects_to').property('direction','down').from('c8').to('c2')");
                sb.Append(".addE('connects_to').property('direction','down').from('c10').to('c3')");
                sb.Append(".addE('connects_to').property('direction','down').from('c12').to('c4')");
                sb.Append(".addE('connects_to').property('direction','down').from('c14').to('c5')");
                //up
                sb.Append(".addE('connects_to').property('direction','up').from('c7').to('c16')");
                sb.Append(".addE('connects_to').property('direction','up').from('c9').to('c17')");
                sb.Append(".addE('connects_to').property('direction','up').from('c11').to('c18')");
                sb.Append(".addE('connects_to').property('direction','up').from('c13').to('c19')");
                sb.Append(".addE('connects_to').property('direction','up').from('c15').to('c20')");

                //third ring c16 - c20
                //right
                sb.Append(".addE('connects_to').property('direction','right').from('c16').to('c17')");
                sb.Append(".addE('connects_to').property('direction','right').from('c17').to('c18')");
                sb.Append(".addE('connects_to').property('direction','right').from('c18').to('c19')");
                sb.Append(".addE('connects_to').property('direction','right').from('c19').to('c20')");
                sb.Append(".addE('connects_to').property('direction','right').from('c20').to('c16')");
                //left
                sb.Append(".addE('connects_to').property('direction','left').from('c20').to('c19')");
                sb.Append(".addE('connects_to').property('direction','left').from('c19').to('c18')");
                sb.Append(".addE('connects_to').property('direction','left').from('c18').to('c17')");
                sb.Append(".addE('connects_to').property('direction','left').from('c17').to('c16')");
                sb.Append(".addE('connects_to').property('direction','left').from('c16').to('c20')");
                //down
                sb.Append(".addE('connects_to').property('direction','down').from('c16').to('c7')");
                sb.Append(".addE('connects_to').property('direction','down').from('c17').to('c9')");
                sb.Append(".addE('connects_to').property('direction','down').from('c18').to('c11')");
                sb.Append(".addE('connects_to').property('direction','down').from('c19').to('c13')");
                sb.Append(".addE('connects_to').property('direction','down').from('c20').to('c15')");

                await gremlinClient.SubmitAsync(sb.ToString());

                //grab all of the ids
                var chambers = (await gremlinClient.SubmitAsync<string>("g.V().hasLabel('chamber').id()")).OrderBy(x=>Guid.NewGuid()).Take(6).ToList();

                sb = new StringBuilder("g");

                sb.Append($".V('{chambers[0]}').as('c1')");
                sb.Append($".V('{chambers[1]}').as('c2')");
                sb.Append($".V('{chambers[2]}').as('c3')");
                sb.Append($".V('{chambers[3]}').as('c4')");
                sb.Append($".V('{chambers[4]}').as('c5')");
                sb.Append($".V('{chambers[5]}').as('c6')");
                //bats(2)
                sb.Append(".addV('bat').property('Name','Bat #1').addE('occupies').to('c1')");
                sb.Append(".addV('bat').property('Name','Bat #2').addE('occupies').to('c2')");
                //pits(2)
                sb.Append(".addV('pit').property('Name','Pit #1').addE('occupies').to('c3')");
                sb.Append(".addV('pit').property('Name','Pit #2').addE('occupies').to('c4')");
                //wumpus
                sb.Append(".addV('wumpus').property('Name','Wumpus').addE('occupies').to('c5')");
                //hunter
                sb.Append(".addV('hunter').property('Name','Hunter').addE('occupies').to('c6')");

                await gremlinClient.SubmitAsync(sb.ToString());

            }            
        }
    }
}
