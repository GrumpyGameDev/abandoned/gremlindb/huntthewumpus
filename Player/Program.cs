﻿using System.Threading;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using Gremlin.Net.Driver;
using Gremlin.Net.Driver.Exceptions;
using Gremlin.Net.Structure.IO.GraphSON;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

namespace Builder
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile("appsettings.local.json", true, true)
                .Build();

            var gremlinServer = new GremlinServer(config["GremlinDB:Host"], 443, enableSsl: true, 
                                                    username: $"/dbs/{config["GremlinDB:Database"]}/colls/{config["GremlinDB:Graph"]}", 
                                                    password: config["GremlinDB:Key"]);

            using (var gremlinClient = new GremlinClient(gremlinServer, new GraphSON2Reader(), new GraphSON2Writer(), GremlinClient.GraphSON2MimeType))
            {
                await RunGame(gremlinClient);
            }
        }

        static async Task RunGame(GremlinClient client)
        {
            bool done = false;
            while(!done)
            {
                //find the hunter
                //TODO: also look for bat, pit, wumpus
                var hunterStats = await client.SubmitWithSingleResultAsync<Dictionary<string,object>>(
                    "g.V().hasLabel('hunter').project('HunterId','ChamberId','ChamberName','Bats','Pits','Wumpuses').by(id()).by(out('occupies').id()).by(out('occupies').values('Name')).by(out('occupies').in('occupies').hasLabel('bats').count()).by(out('occupies').in('occupies').hasLabel('pit').count()).by(out('occupies').in('occupies').hasLabel('wumpus').count())"
                    );

                Console.WriteLine($"You are in {hunterStats["ChamberName"]}");

                var chamberExits = await client.SubmitAsync<Dictionary<string,object>>(
                    $"g.V('{hunterStats["ChamberId"]}').outE('connects_to').project('Direction','DestinationId').by(values('direction')).by(inV().id())"
                );

                Console.WriteLine("Exits:");
                foreach(var chamberExit in chamberExits)
                {
                    Console.WriteLine($"\t{chamberExit["Direction"]}");
                }
            }
        }
    }
}
